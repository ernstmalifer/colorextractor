const fs = require('fs');
const colorExtractor = require('img-color-extractor');

stream = fs.createReadStream('in2.png')

defaultsOptions = {
    background: '#FFFFFF',
    alphaMin: 0,
    dist: 100,
    greyVa: -1,
};

// retun a promise resolving an array of objects as:
// [{ color: `hexa`, n: `numberOfOccurrence`, r: `ratio` }, ...]
colorExtractor.extract(stream, defaultsOptions)
.then(colors => {
    console.log(colors) // [ { color: '#ffffff', n: 1515551, r: 0.728368706 },
                        //   { color: '#333333', n: 388783, r: 0.1868478003 },
                        //   { color: '#669f64', n: 174227, r: 0.0837329094 },
                        //   { color: '#b2ceb1', n: 2186, r: 0.0010505842 } ]
});